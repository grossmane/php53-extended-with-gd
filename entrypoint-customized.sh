#!/bin/bash

##########################################################################
# >> DEFAULT VARS
#
# add them here. 
# Example: DEFAULT_VARS["KEY"]="VALUE"
##########################################################################
declare -A DEFAULT_VARS
DEFAULT_VARS["SSMTP_ROOT"]="${SSMTP_ROOT:=""}"
DEFAULT_VARS["SSMTP_MAILHUB"]="${SSMTP_MAILHUB:="mail:587"}"
DEFAULT_VARS["SSMTP_REWRITEDOMAIN"]="${SSMTP_REWRITEDOMAIN:=""}"
DEFAULT_VARS["SSMTP_HOSTNAME"]="${SSMTP_HOSTNAME:=""}"
DEFAULT_VARS["SSMTP_USESTARTTLS"]="${SSMTP_USESTARTTLS:="YES"}"
DEFAULT_VARS["SSMTP_AUTHUSER"]="${SSMTP_AUTHUSER:=""}"
DEFAULT_VARS["SSMTP_AUTHPASS"]="${SSMTP_AUTHPASS:=""}"
DEFAULT_VARS["SSMTP_AUTHMETHOD"]="${SSMTP_AUTHMETHOD:="LOGIN"}"
DEFAULT_VARS["SSMTP_FROMLINEOVERRIDE"]="${SSMTP_FROMLINEOVERRIDE:="NO"}"
##########################################################################
# << DEFAULT VARS
##########################################################################

sed -i 's/%%ROOT%%/'${DEFAULT_VARS["SSMTP_ROOT"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%MAILHUB%%/'${DEFAULT_VARS["SSMTP_MAILHUB"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%REWRITEDOMAIN%%/'${DEFAULT_VARS["SSMTP_REWRITEDOMAIN"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%HOSTNAME%%/'${DEFAULT_VARS["SSMTP_HOSTNAME"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%USESTARTTLS%%/'${DEFAULT_VARS["SSMTP_USESTARTTLS"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%AUTHUSER%%/'${DEFAULT_VARS["SSMTP_AUTHUSER"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%AUTHPASS%%/'${DEFAULT_VARS["SSMTP_AUTHPASS"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%AUTHMETHOD%%/'${DEFAULT_VARS["SSMTP_AUTHMETHOD"]}'/g' /etc/ssmtp/ssmtp.conf
sed -i 's/%%FROMLINEOVERRIDE%%/'${DEFAULT_VARS["SSMTP_FROMLINEOVERRIDE"]}'/g' /etc/ssmtp/ssmtp.conf

# Starting cron
/bin/sh -c /usr/sbin/cron -f

/entrypoint.sh $@
exit 0
